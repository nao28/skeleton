from flask import Flask, jsonify, request
from . import model
from .calculate import add, sqrt

app = Flask(__name__)


@app.route('/<uname>')
def hello_world(uname):
    user = model.User.query.filter_by(username=uname).first()
    return(jsonify({'message': 'Hello %s!' % uname,
                    'email': user.email,
                    'id': user.id}))


@app.route('/math/add')
def do_add():
    """
    Add two arguments provided in the querystring.
    It is intentional that this code doesn't function
    as expected yet.
    """
    a = request.args.get('a')
    b = request.args.get('b')
    return jsonify({'message': 'successful', 'result': add(a, b)})


@app.route('/math/sqrt')
def do_sqrt():
    """
    Take the square root of an argument provided in the querystring.
    It is intentional that this code doesn't function
    as expected yet.
    """
    a = request.args.get('a')
    return jsonify({'message': 'successful', 'result': sqrt(a)})
