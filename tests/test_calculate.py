from unittest import TestCase
from my_tdd_app import calculate


class test_calculate(TestCase):
    def test_add(self):
        """
        Test the add function. It should add the two arguments together.
        """
        result = calculate.add(5, 3)
        self.assertEqual(result, 8)

    def test_sqrt(self):
        """
        Test the sqrt function.
        """
        result = calculate.sqrt(9)
        self.assertEqual(result, 3)
